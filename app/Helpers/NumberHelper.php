<?php
/**
 * Created by PhpStorm.
 * User: Tuan
 * Date: 6/13/2018
 * Time: 8:47 AM
 */

namespace App\Helpers;


class NumberHelper
{

    public static function convertNumberToString($n)
    {
        $collection = [
            0 => ['0', 'zero', 'z3ro'],
            1 => ['1', 'one', '0ne'],
            2 => ['2', 'two', 'tw0'],
            3 => ['3', 'three', 'thr33'],
            4 => ['4', 'four', 'f0ur'],
            5 => ['5', 'five', 'flve'],
            6 => ['6', 'six', 'slx'],
            7 => ['7', 'seven', 's3v3n'],
            8 => ['8', 'eight', 'elght'],
            9 => ['9', 'nine', 'nlne'],
            10 => ['10', 'ten', 't3n'],
            11 => ['11', 'eleven'],
            12 => ['12', 'twelve'],
            13 => ['13', 'thirteen'],
            14 => ['14', 'fourteen'],
            15 => ['15', 'fifteen'],
            16 => ['16', 'sixteen'],
            17 => ['17', 'seventeen'],
            18 => ['18', 'eighteen'],
            19 => ['19', 'nineteen'],
            20 => ['20', 'twenty'],
        ];

        if (!isset($collection[$n])) {
            return $n;
        }

        $item = collect($collection[$n]);

        return $item->random();
    }

    public static function convertMathOperatorToString($operator)
    {
        $collection = [
            '+' => ['+', 'plus'],
            '-' => ['-', 'minus'],
            '*' => ['*', 'times'],
        ];

        if (!isset($collection[$operator])) {
            return $operator;
        }

        $item = collect($collection[$operator]);

        return $item->random();
    }

}