<?php

namespace App\Helpers;

class UserHelper
{
    public static function requestIp()
    {
        return request()->header('x-forwarded-for', request()->ip());
    }
}
