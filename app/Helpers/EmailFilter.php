<?php

namespace App\Helpers;

class EmailFilter
{
    public static function filterAlias($email)
    {
        $gmail = '@gmail.com';
        $email = strtolower($email);

        if (substr($email, -10) === $gmail) {
            $gName = substr($email, 0, -10);
            $gName = str_replace('.', '', $gName);
            $plusPos = strpos($gName, '+');
            if ($plusPos) {
                $gName = substr($gName, 0, $plusPos);
            }

            $email = $gName . $gmail;
        }

        return $email;
    }
}
