<?php

namespace App\Models;

class TransactionInfo extends BaseModel
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'transaction_info';

}
