<?php
/**
 * Created by PhpStorm.
 * User: Tuan
 * Date: 6/10/2018
 * Time: 13:59
 */

namespace App\Models;

use Illuminate\Database\Query\Builder;

trait QueryHelper
{

    /**
     * @param $query Builder
     * @param $column string
     * @param $operator string
     * @param $value mixed
     * @return Builder
     */
    public function scopeFilterWhere($query, $column, $operator, $value)
    {
        if ($value) {
            if ($operator === 'like') {
                //$value = '%' . $value . '%';
            }

            $query->where($column, $operator, $value);
        }

        return $query;
    }
}
