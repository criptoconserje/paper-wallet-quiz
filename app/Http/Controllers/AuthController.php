<?php

namespace App\Http\Controllers;

use App\Helpers\EmailFilter;
use App\Helpers\UserHelper;
use App\Models\BannedEmailProvider;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $credentials['email'] = EmailFilter::filterAlias($credentials['email']);

        $isLoggedIn = $this->guard()->attempt($credentials);

        if (!$isLoggedIn) {
            $request->session()->flash('login-message', 'Login failed! Please try again.');

            return response()
                ->redirectToAction('AuthController@showLoginForm');
        }

        if (auth()->user()->is_banned) {
            $this->guard()->logout();
            $request->session()->flash('login-message', 'Your account has been banned. If you think you\'ve been wrongly banned, please contact @rocket on Discord.');

            return response()
                ->redirectToAction('AuthController@showLoginForm');
        }

        // check email provider banned
        $providerDomain = substr($credentials['email'], strpos($credentials['email'], '@') + 1);
        if (BannedEmailProvider::checkBannedDomain($providerDomain)) {
            $this->guard()->logout();
            $request->session()->flash('login-message', 'Your account has been banned. If you think you\'ve been wrongly banned, please contact @rocket on Discord.');

            return response()
                ->redirectToAction('AuthController@showLoginForm');
        }
        // end check


        $request->session()->flash('first_login', '1');

        return redirect('/');
    }

    public function showRegisterForm()
    {
        return view('auth.register');
    }

    public function register(Request $request)
    {
        $requestData = $request->input();
        $originalEmail = $requestData['email'];
        $requestData['email'] = EmailFilter::filterAlias($requestData['email']);
        $requestIp = UserHelper::requestIp();

        $validator = Validator::make($requestData, [
            'first_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed',
            'subscribe' => 'required',
        ]);

        if ($validator->fails()) {
            $requestData['email'] = $originalEmail;
            return response()
                ->redirectToAction('AuthController@showRegisterForm')
                ->withInput($requestData)
                ->withErrors($validator);
        }

        // check IP duplicate
        if (User::where('ip_register', '=', $requestIp)->exists()) {

            $request->session()->flash('register-message', 'There is already a registered account with your IP in the system. You can not register a new account with this IP');

            return response()
                ->redirectToAction('AuthController@showRegisterForm')
                ->withInput($requestData);
        }

        // check email provider banned
        $providerDomain = substr($requestData['email'], strpos($requestData['email'], '@') + 1);
        if (BannedEmailProvider::checkBannedDomain($providerDomain)) {

            return response()
                ->redirectToAction('AuthController@showRegisterForm')
                ->withErrors(['email' => 'This email provider is banned'])
                ->withInput($requestData);
        }

        $requestData['password'] = bcrypt($requestData['password']);
        $user = new User($requestData);
        $user->confirmation_code = str_random(40);
        $user->remember_token = str_random(10);
        $user->ip_register = $requestIp;

        $user->save();

        $this->guard()->login($user);
        $request->session()->flash('first_login', '1');

        Mail::to($user->email)->send(new \App\Mail\RegisterMail($user));

        return redirect('/');
    }

    public function verify(Request $request)
    {
        $pId = $request->input('id');
        $pCode = $request->input('code');

        $user = User::find($pId);
        $isConfirmed = false;

        if ($user && $user->confirmation_code === $pCode) {
            $user->confirmation_code = str_random(40);

            $user->is_confirmed = 1;

            if ($user->subscribeMaillingList()) {
                $user->is_subscribed = 1;
            }

            $user->save();

            $isConfirmed = true;

            $this->guard()->login($user);
        }

        return view('auth.verify', ['isConfirmed' => $isConfirmed]);
    }

    public function showForgotPasswordForm()
    {
        return view('auth.forgot_password');
    }

    public function forgotPassword(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email',
        ]);

        $email = EmailFilter::filterAlias($request->input('email'));

        $user = User::where('email', '=', $email)->first();

        if (!$user) {
            return view('components.message', [
                'title' => 'Forgot password',
                'alert' => 'warning',
                'message' => 'Email not found in system',
            ]);
        }

        $user->confirmation_code = str_random(40);
        $user->save();

        Mail::to($user->email)->send(new \App\Mail\ResetMail($user));

        return view('components.message', [
            'title' => 'Forgot password',
            'alert' => 'success',
            'message' => 'Please check your email to reset password',
        ]);
    }

    public function showResetPasswordForm(Request $request)
    {
        $pId = $request->input('id');
        $pCode = $request->input('code');

        $user = User::find($pId);

        if (!$user || $user->confirmation_code !== $pCode) {
            return view('components.message', [
                'title' => 'Reset password',
                'alert' => 'warning',
                'message' => 'Invalid token',
            ]);
        }

        return view('auth.reset_password', [
            'pId' => $pId,
            'pCode' => $pCode,
        ]);
    }

    public function resetPassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|string|confirmed',
        ]);

        $pId = $request->input('id');
        $pCode = $request->input('code');

        $user = User::find($pId);

        if (!$user || $user->confirmation_code !== $pCode) {
            return view('components.message', [
                'title' => 'Reset password',
                'alert' => 'warning',
                'message' => 'Invalid token',
            ]);
        }

        $newPassword = bcrypt($request->input('password'));
        $user->password = $newPassword;
        $user->confirmation_code = str_random(40);
        $user->save();

        return view('components.message', [
            'title' => 'Reset password',
            'alert' => 'success',
            'message' => 'Password reset successfully!',
        ]);
    }

    public function resetActivationEmail()
    {
        if (!auth()->check()) return response()->redirectToAction('AuthController@showLoginForm');

        /** @var User $user */
        $user = auth()->user();

        if ($user->is_confirmed) {
            return redirect('/');
        }

        $user->confirmation_code = str_random(40);
        $user->save();

        request()->session()->flash('resent_verify_email', '1');

        Mail::to($user->email)->send(new \App\Mail\RegisterMail($user));

        return redirect('/');
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }

    public function subscribe()
    {
        if (!auth()->check()) return response()->redirectToAction('AuthController@showLoginForm');

        $user = auth()->user();

        if ($user->subscribeMaillingList()) {
            $user->is_subscribed = 1;
        }

        $user->save();
        return redirect('/');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

}
