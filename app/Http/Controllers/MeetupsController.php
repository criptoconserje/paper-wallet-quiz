<?php

namespace App\Http\Controllers;

use App\Models\TransactionInfo;
use App\Services\EasyZenCash;
use App\Services\MailChimpService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MeetupsController extends Controller
{
    const USD_AMOUNT_DEFAULT = '0.5';

    const SESSION_ZEN_USD_RATE = 'zen_usd_rate';

    const SESSION_USD_AMOUNT_KEY = 'usd_amount';
    const SESSION_USD_CUSTOM_AMOUNT_KEY = 'usd_custom_amount';
    const SESSION_ZEN_CUSTOM_AMOUNT_KEY = 'zen_custom_amount';

    const SESSION_EVENT_NAME_KEY = 'event_name';
    const SESSION_MEMBER_EMAIL_KEY = 'member_email';

    public function __construct()
    {
        $this->middleware('user.login')->except(['showLoginForm', 'login']);
    }

    public function index()
    {
        return $this->showSubscribeForm();
    }

    public function showLoginForm()
    {
        return view('meetups.login');
    }

    public function login(Request $request)
    {
        $isLoggedIn = $this->guard()->attempt($request->only('username', 'password'));

        if (!$isLoggedIn) {
            $request->session()->flash('login-message', 'Login failed! Please try again.');

            return response()->redirectToAction('MeetupsController@showLoginForm');
        }

        try {
            // get & save rate of ZEN-USD
            $Btc_Usd = json_decode(file_get_contents('https://bittrex.com/api/v1.1/public/getticker?market=USD-BTC'), true);
            $Btc_Zen = json_decode(file_get_contents('https://bittrex.com/api/v1.1/public/getticker?market=BTC-ZEN'), true);

            $priceBTCinUSD = $Btc_Usd['result']['Last'];
            $priceZENinBTC = $Btc_Zen['result']['Last'];

            $priceZENinUSD = $priceBTCinUSD * $priceZENinBTC;
            $priceZENinUSD = round($priceZENinUSD, 8);

            session([self::SESSION_ZEN_USD_RATE => $priceZENinUSD]);

        } catch (\Exception $e) {
            $request->session()->flash('login-message', 'Error when get ZEN-USD rate. Msg: ' . $e->getMessage());

            return response()->redirectToAction('MeetupsController@showLoginForm');
        }


        session([self::SESSION_EVENT_NAME_KEY => $request->input('event')]);

        return response()->redirectToAction('MeetupsController@index');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return response()->redirectToAction('MeetupsController@index');
    }

    public function showSubscribeForm()
    {
        return view('meetups.subscribe');
    }

    public function subscribe(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email',
//            'first_name' => 'required',
        ]);

        $email = $request->input('email');
        $firstName = $request->input('first_name', 'Member');

        $data = [
            'email' => $email,
            'status' => 'subscribed',
            'firstname' => $firstName,
            'lastname' => $firstName,
        ];

        // TODO uncomment
        $code = MailChimpService::subscribe($data);

        session([self::SESSION_MEMBER_EMAIL_KEY => $email]);

        return response()->redirectToAction('MeetupsController@showClaimForm');
    }

    public function showClaimForm()
    {
        return view('meetups.claim', [
            'memberEmail' => session(self::SESSION_MEMBER_EMAIL_KEY),
            'eventName' => session(self::SESSION_EVENT_NAME_KEY),
            'amountSetting' => $this->getSettingAmount(),
        ]);
    }

    public function claim(Request $request)
    {
        $zenAddress = $request->input('zen_address');

        $zenAmount = $request->input('zen_amount');
        //$zenAmount = 0.00001; // TODO testing

        $emailSubscribed = $request->input('member_email');
        $eventName = $request->input('event_name');

        $ZenCash = $this->getEasyZenCash();

        $response = $ZenCash->validateaddress($zenAddress);

        if (!$response['isvalid']) {
            return response()->json([
                'status' => 'ok',
                'msg' => 'The Zen Address you entered is not Valid, Try Again with a Valid Address',
            ]);
        }

        $transferResponse = $ZenCash->sendtoaddress($zenAddress, $zenAmount);

        if ($transferResponse == null) {
            return response()->json([
                'status' => 'ok',
                'msg' => 'An error has occurred. Try Again Later. ZenCash only accepts transparent addresses',
            ]);
        }

        $transaction = new TransactionInfo();
        $transaction->admin_id = $this->guard()->user()->id;
        $transaction->team_member = $this->guard()->user()->username;
        $transaction->amount_given = $zenAmount;
        $transaction->zen_address = $zenAddress;
        $transaction->email_address_subscribed = $emailSubscribed;
        $transaction->event_name = $eventName;
        $transaction->save();

        return response()->json([
            'status' => 'ok',
            'msg' => $zenAmount . ' ZEN transferred to address ' . $zenAddress,
        ]);
    }

    public function showSettingForm()
    {
        $zenAmount = session(self::SESSION_USD_AMOUNT_KEY, self::USD_AMOUNT_DEFAULT);
        $customUsdAmount = session(self::SESSION_USD_CUSTOM_AMOUNT_KEY);
        $customZenAmount = session(self::SESSION_ZEN_CUSTOM_AMOUNT_KEY);
        $eventName = session(self::SESSION_EVENT_NAME_KEY);

        return view('meetups.setting', [
            'zenAmount' => $zenAmount,
            'customUsdAmount' => $customUsdAmount,
            'customZenAmount' => $customZenAmount,
            'eventName' => $eventName,
        ]);
    }

    public function saveSetting(Request $request)
    {
        session([self::SESSION_USD_AMOUNT_KEY => $request->input('zen_amount')]);
        session([self::SESSION_USD_CUSTOM_AMOUNT_KEY => $request->input('zen_amount_custom_usd')]);
        session([self::SESSION_ZEN_CUSTOM_AMOUNT_KEY => $request->input('zen_amount_custom_zen')]);

        session([self::SESSION_EVENT_NAME_KEY => $request->input('event_name')]);

        return response()->redirectToAction('MeetupsController@index');
    }

    public function getBalance()
    {
        $ZenCash = $this->getEasyZenCash();

        $balance = $ZenCash->getbalance();

        return $balance;
    }

    private function getSettingAmount()
    {
        $zenRate = session(self::SESSION_ZEN_USD_RATE);
        $usdAmount = session(self::SESSION_USD_AMOUNT_KEY, self::USD_AMOUNT_DEFAULT);

        if ($usdAmount === 'custom') {

            $usdAmount = session(self::SESSION_USD_CUSTOM_AMOUNT_KEY);
            $zenAmount = session(self::SESSION_ZEN_CUSTOM_AMOUNT_KEY);

            if ($usdAmount) {
                $zenAmount = round($usdAmount / $zenRate, 8);
            } elseif ($zenAmount) {
                $usdAmount = round($zenAmount * $zenRate, 8);
            } else {
                $usdAmount = self::USD_AMOUNT_DEFAULT;
                $zenAmount = round($usdAmount / $zenRate, 8);
            }

        } else { // select box (by USD)
            $zenAmount = round($usdAmount / $zenRate, 8);
        }

        return [
            'zenRate' => $zenRate,
            'zenAmount' => $zenAmount,
            'usdAmount' => $usdAmount,
        ];
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        //return Auth::guard('admin');
        return auth()->guard();
    }

    /**
     * @return EasyZenCash
     */
    protected function getEasyZenCash()
    {
        return new EasyZenCash(env('EASYZENCASH_USERNAME'), env('EASYZENCASH_PASSWORD'), env('EASYZENCASH_HOST'), env('EASYZENCASH_PORT'));
    }

}
