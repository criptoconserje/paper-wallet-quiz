<?php

namespace App\Http\Controllers\Admin;

use App\Models\TransactionInfo;
use App\Models\ZenAddress;

class HomeController
{
    public function index()
    {

        $topTransactionInfo = TransactionInfo::query()
            ->select([
                'zen_address',
                'email_address_subscribed',
                \DB::raw('SUM(`amount_given`) AS `total_amount_given`')
            ])
            ->groupBy(['zen_address', 'email_address_subscribed'])
            ->orderBy('total_amount_given', 'DESC')
            ->limit(10)
            ->get()
            ->toArray();

        $topTransactionInfoByEvent = [];
        $a = TransactionInfo::query()
            ->select([
                'event_name',
                'team_member',
                \DB::raw('SUM(`amount_given`) AS `total_amount_given`')
            ])
            ->groupBy(['event_name', 'team_member'])
            ->orderBy('total_amount_given', 'DESC')
            ->limit(10)
            ->get()
            ->toArray();
        foreach ($a as $v) {
            $topTransactionInfoByEvent[$v['event_name']]['team_member'] = $v['total_amount_given'];
        }

        return view('admin.home.index', [
            'topTransactionInfo' => $topTransactionInfo,
            'topTransactionInfoByEvent' => $topTransactionInfoByEvent,
        ]);
    }
}
