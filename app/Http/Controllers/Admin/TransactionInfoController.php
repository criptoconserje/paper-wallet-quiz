<?php

namespace App\Http\Controllers\Admin;

use App\Models\TransactionInfo;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /** @var Builder $query */
        $query = TransactionInfo::query();
//            ->filterWhere('zen_address', '=', $request->input('zen_address'))
//            ->filterWhere('team_member', '=', $request->input('team_member'))
//            ->filterWhere('email_address_subscribed', '=', $request->input('email_address_subscribed'))
//            ->filterWhere('event_name', '=', $request->input('event_name'))
//            ->filterWhere('created_at', '>=', $request->input('created_at_from'))
//            ->filterWhere('created_at', '<=', $request->input('created_at_to'));

        $stats = [
            'sum' => $query->sum('amount_given'),
            'max' => $query->max('amount_given'),
            'min' => $query->min('amount_given'),
        ];

        $models = $query->orderBy('id', 'DESC')
            ->paginate();

        return view('admin.transaction-info.index', [
            'models' => $models,
            'stats' => $stats,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
