<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\EmailFilter;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::query()
//            ->filterWhere('username', '=', $request->input('username'))
//            ->filterWhere('created_at', '>=', $request->input('created_at_from'))
//            ->filterWhere('created_at', '<=', $request->input('created_at_to'))
//            ->orderBy('id', 'DESC')
            ->paginate();

        return view('admin.user.index', [
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|string|max:255|unique:users',
            'password' => 'required|string',
            'is_admin' => 'boolean',
        ]);

        $user = new User($request->input());
        $user->password = bcrypt($user->password);
        $user->remember_token = str_random(10);
        $user->is_admin = (int)$request->input('is_admin');

        $user->save();

        return response()->redirectToRoute('admin.user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('admin.user.edit', ['model' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $this->validate($request, [
            'username' => 'required|string|max:255|unique:users,username,' . $user->id,
            'password' => 'nullable|string',
            'is_admin' => 'boolean',
        ]);

        $user->username = $request->input('username');
        if ($request->has('password')) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->is_admin = (int)$request->input('is_admin');

        $user->save();

        return response()->redirectToRoute('admin.user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $user->delete();

        return response()->redirectToRoute('admin.user.index');
    }

    public function massAction(Request $request)
    {
        $ids = $request->input('item_ids', []);
        $act = $request->input('mass_action');

        /** @var User[] $models */
        $models = User::whereIn('id', $ids)->get();

        switch ($act) {
            case 'ban':
                foreach ($models as $model) {
                    $model->is_banned = 1;
                    $model->save();
                }
                break;
            case 'delete':
                foreach ($models as $model) {
                    $model->delete();
                }
                break;
        }

        return response()->redirectToRoute('admin.user.index');
    }

}
