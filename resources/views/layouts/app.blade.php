<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css?family=Karla:400,700|Roboto+Condensed:400,700" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
          integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous"/>

    <title>ZenCash</title>

    <style>
        body {
            background-image: url("{{ asset('assets/img/background_xs.png') }}");
            background-repeat: no-repeat;
            background-color: #0b284d;
            background-size: cover;
            background-position: center;
            width: 100vw;
            height: 100vh;
            color: white;
        }

        .title {
            font-size: 1.75rem;
            margin-bottom: 1.5rem;
            margin-top: 1.5rem;
            text-align: center;
            font-family: 'Roboto Condensed', sans-serif;
        }

        .camera-option {
            display: block;
            padding: 10px 0;
        }

        @media (min-width: 992px) {
            body {
                background-image: url("{{ asset('assets/img/background_md.png') }}");
            }

            .title {
                font-size: 2.75rem;
                margin-top: 2rem;
                margin-bottom: 3.5rem;
            }
        }

        a, a:hover {
            color: white;
        }

        .form-control, .input-group-text, .btn {
            border-radius: 0;
        }

        .modal, .modal a {
            color: #000;
        }

        .modal a.btn-success {
            color: white;
        }

        .karla {
            font-family: 'Karla', sans-serif;
        }
    </style>
    @stack('styles')

</head>
<body>

<!-- Page Content -->
<div class="container">

    @if(auth()->check())
        <div class="text-right pt-1">
            @if(isset($showAdminLink) && $showAdminLink && auth()->user()->is_admin)
                <a href="{{ route('admin.home') }}" class="d-inline-block">
                    <i class="fa fa-2x fa-cogs"></i>
                </a>
            @endif

            <a href="{{ action('MeetupsController@showSettingForm') }}" class="d-inline-block ml-2">
                <i class="fa fa-2x fa-cog"></i>
            </a>

            <a href="{{ action('MeetupsController@logout') }}" class="d-inline-block ml-2">
                <i class="fa fa-2x fa-power-off"></i>
            </a>
        </div>
    @endif

    @yield('content')

</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
        integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"
        integrity="sha256-1A78rJEdiWTzco6qdn3igTBv9VupN3Q1ozZNTR4WE/Y=" crossorigin="anonymous"></script>

@stack('scripts')

</body>
</html>
