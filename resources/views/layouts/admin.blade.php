<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>

    <title>ZenCash Faucet Admin</title>

    @stack('styles')

</head>
<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="{{ route('admin.home') }}">
            Admin
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.user.index') }}">Users</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.transaction-info.index') }}">Transaction Info</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ action('MeetupsController@logout') }}?t={{ time() }}">Logout
                        ({{ auth()->user()->username }})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ action('MeetupsController@index') }}">Meetups</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container-fluid mt-2">

    @yield('content')

</div>


{{ Form::open(['method' => 'DELETE', 'class' => 'global-form-delete']) }}
{{ Form::close() }}


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
        integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.7.3/feather.min.js"
        integrity="sha256-km9ViEDg2jYMRIV3IgvhZkrGIXt7e+T0zYez9FC064c=" crossorigin="anonymous"></script>
<script>
    feather.replace()
</script>

<script>
    $(function () {

        $(".btn-delete").click(function () {
            var btn = $(this);
            if (confirm(btn.data("confirm"))) {
                var form = $(".global-form-delete");
                form.attr("action", btn.data("url"));
                form.submit();
            }
        });

        $(".date-range-picker").each(function (_, inputPicker) {
            inputPicker = $(inputPicker);

            inputPicker.daterangepicker({
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 3 Days': [moment().subtract(2, 'days'), moment()],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'This Year': [moment().startOf('year'), moment().endOf('year')],
                    'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
                },
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                },
                showDropdowns: true
            }, function (start, end, label) {
                $(inputPicker.data("input-from")).val(start.format('YYYY-MM-DD'));
                $(inputPicker.data("input-to")).val(end.format('YYYY-MM-DD'));
            });

            inputPicker.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' => ' + picker.endDate.format('MM/DD/YYYY'));
            });

            inputPicker.on('cancel.daterangepicker', function(ev, picker) {
                inputPicker.val("");
                $(inputPicker.data("input-from")).val("");
                $(inputPicker.data("input-to")).val("");
            });
        });

    });
</script>

@stack('scripts')

</body>
</html>
