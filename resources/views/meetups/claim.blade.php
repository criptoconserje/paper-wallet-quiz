@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-6 offset-md-3">

            <h3 class="title">Get free ZEN!</h3>

            @if($errors->any())
                <div class="alert alert-danger">
                    <ul class="mb-0">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div>
                <form action="{{ action('MeetupsController@claim') }}" method="post" id="form-claim" autocomplete="off">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" id="zen_address" name="zen_address" class="form-control" placeholder="Zen Address">
                            <div class="input-group-append">
                                <button class="btn btn-success" type="button" id="btn-scan-qr-code">
                                    <i class="fa fa-2x fa-qrcode"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        Email subscribed: <b>{{ $memberEmail }}</b>
                        <input type="hidden" name="member_email" value="{{ $memberEmail }}"/>
                    </div>

                    <div class="form-group">
                        Event: <b>{{ $eventName }}</b>
                        <input type="hidden" name="event_name" value="{{ $eventName }}"/>
                    </div>

                    <div class="form-group">
                        Amount: <b>${{ $amountSetting['usdAmount'] }}</b>
                        ({{ $amountSetting['zenAmount'] }} ZEN)
                        <input type="hidden" name="zen_amount" value="{{ $amountSetting['zenAmount'] }}"/>
                    </div>

                    <div class="form-group">
                        ZEN/USD rate: <b>${{ number_format($amountSetting['zenRate'], 2) }}</b>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <button type="submit" class="btn btn-success btn-block">Claim</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="modal-scan-qr-code" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Scan QR Code</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="embed-responsive embed-responsive-1by1">
                            <video id="preview" class="embed-responsive-item"></video>
                        </div>
                        <div class="mt-2">
                            <a href="#" id="btn-switch-camera">Switch camera</a>
                        </div>
                    </div>
                    <div id="scan-result"></div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="modal-choose-camera" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Choose camera</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="list-camera"></div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="modal-claim-message" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Message</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="claim-message"></div>

                    <div class="row mt-4">
                        <div class="col-8 offset-2">
                            <a href="{{ action('MeetupsController@index') }}" class="btn btn-success btn-block">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection

@push('scripts')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/3.3.3/adapter.min.js"></script>

    <script type="text/javascript" src="{{ asset('assets/js/instascan.min.js') }}"></script>
    <script type="text/javascript">

        $(function () {

            var CK_CAMERA_ID_NAME = "_selected_camera_id";
            var i, cam, scanner, listCameras = [];

            scanner = new Instascan.Scanner({video: document.getElementById('preview'), scanPeriod: 5});

            scanner.addListener('scan', function (content) {
                $("#modal-scan-qr-code").modal("hide");

                var prefix = "zencash:";

                if (content.substr(0, prefix.length) === prefix) {
                    content = content.substr(prefix.length);
                }

                $("#zen_address").val(content);
            });

            Instascan.Camera.getCameras().then(function (cameras) {
                if (cameras.length > 0) {
                    listCameras = cameras;
                } else {
                    alert('No cameras found.');
                }
            }).catch(function (e) {
                console.error(e);
            });

            $("#btn-scan-qr-code").click(function () {
                var cameraId = $.cookie(CK_CAMERA_ID_NAME);

                if (cameraId) {

                    for (i = 0; i < listCameras.length; i++) {
                        cam = listCameras[i];
                        if (cam.id === cameraId) {
                            scanner.start(cam);
                            $("#modal-scan-qr-code").modal("show");
                            return;
                        }
                    }

                }

                $("#modal-choose-camera").modal("show");
            });

            $("#list-camera").on("click", ".camera-option", function () {
                $("#modal-choose-camera").modal("hide");
                $.cookie(CK_CAMERA_ID_NAME, $(this).data("id"), {expires: 365, path: '/'});
                $("#btn-scan-qr-code").click();
            });

            $("#btn-switch-camera").click(function (e) {
                e.preventDefault();
                $("#modal-scan-qr-code").modal("hide");
                $("#modal-choose-camera").modal("show");
            });

            $("#modal-choose-camera").on("show.bs.modal", function () {
                var elList = $("#list-camera").empty();
                for (i = 0; i < listCameras.length; i++) {
                    cam = listCameras[i];
                    elList.append("<a class='camera-option' data-id='" + cam.id + "'>" + cam.name + "</a>");
                }
            });

            $("#form-claim").submit(function (e) {
                e.preventDefault();
                var form = $(this);
                $.post(form.attr("action"), form.serialize(), function (data) {
                    $("#claim-message").html(data.msg);
                    $("#modal-claim-message").modal("show");
                });
            });

            // // Older browsers might not implement mediaDevices at all, so we set an empty object first
            // if (navigator.mediaDevices === undefined) {
            //     navigator.mediaDevices = {};
            // }
            //
            // // Some browsers partially implement mediaDevices. We can't just assign an object
            // // with getUserMedia as it would overwrite existing properties.
            // // Here, we will just add the getUserMedia property if it's missing.
            // if (navigator.mediaDevices.getUserMedia === undefined) {
            //     navigator.mediaDevices.getUserMedia = function (constraints) {
            //
            //         // First get ahold of the legacy getUserMedia, if present
            //         var getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
            //
            //         // Some browsers just don't implement it - return a rejected promise with an error
            //         // to keep a consistent interface
            //         if (!getUserMedia) {
            //             return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
            //         }
            //
            //         // Otherwise, wrap the call to the old navigator.getUserMedia with a Promise
            //         return new Promise(function (resolve, reject) {
            //             getUserMedia.call(navigator, constraints, resolve, reject);
            //         });
            //     }
            // }
            //
            //
            // var constraints = {
            //     // video: { facingMode: "environment" }
            //     video: true
            // };
            //
            // function handleSuccess(stream) {
            //     var scanner = new Instascan.Scanner({video: document.getElementById('preview'), scanPeriod: 5});
            //     scanner.addListener('scan', function (content) {
            //         $("#scan-result").html(content);
            //         $("#btn-use-result").data("result", content);
            //     });
            //
            //     Instascan.Camera.getCameras().then(function (cameras) {
            //         if (cameras.length > 0) {
            //             if (cameras[1]) {
            //                 scanner.start(cameras[1]);
            //             } else {
            //                 scanner.start(cameras[0]);
            //             }
            //
            //             // for (var i = 0; i < cameras.length; i++) {
            //             //     if (cameras[i].name.indexOf('back') !== -1) {
            //             //         scanner.start(cameras[i]);
            //             //     }
            //             // }
            //         } else {
            //             console.error('No cameras found.');
            //         }
            //     }).catch(function (e) {
            //         console.error(e);
            //     });
            // }
            //
            // function handleError(error) {
            //     console.error('Reeeejected!', error);
            //     alert("No camera permission");
            // }
            //
            // navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess).catch(handleError);
        });

    </script>
@endpush
