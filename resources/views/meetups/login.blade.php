@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-6 offset-md-3">
            <h3 class="title">Zen Team Login Area</h3>

            @if(Session::has('login-message'))
                <div class="alert alert-danger">
                    {{ Session::get('login-message') }}
                </div>
            @endif

            <div>
                <form action="{{ action('MeetupsController@login') }}" method="post" autocomplete="off">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <input type="text" name="username" class="form-control" placeholder="Username">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                        <input type="text" name="event" class="form-control" placeholder="Event in which you're using the APP">
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <button type="submit" class="btn btn-success btn-block">Login</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
