@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-6 offset-md-3">

            <h3 class="title">Get free ZEN!</h3>

            @if($errors->any())
                <div class="alert alert-danger">
                    <ul class="mb-0">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div>
                <form action="{{ action('MeetupsController@subscribe') }}" method="post" autocomplete="off">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <input type="text" name="first_name" class="form-control" placeholder="First name">
                        <input type="text" name="email" class="form-control" placeholder="Email">
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <button type="submit" class="btn btn-success btn-block">Join the community</button>
                            </div>
                        </div>
                    </div>

                    <p class="font-italic small karla">
                        By giving us your email you agree to receive marketing from ZenCash
                    </p>
                </form>
            </div>
        </div>
    </div>

@endsection
