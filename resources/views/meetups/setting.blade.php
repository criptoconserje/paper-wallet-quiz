@extends('layouts.app', ['showAdminLink' => true])

@section('content')

    <div class="row">
        <div class="col-md-6 offset-md-3">

            <h3 class="title">Settings</h3>


        @if($errors->any())
                <div class="alert alert-danger">
                    <ul class="mb-0">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div>
                <form action="{{ action('MeetupsController@saveSetting') }}" method="post" autocomplete="off">
                    {{ csrf_field() }}

                    <div class="form-group">
                        Balance: <b id="account-balance">loading...</b> ZEN
                    </div>

                    <div class="form-group">
                        Zen Address:
                        <button class="btn btn-sm btn-success" type="button" data-toggle="modal" data-target="#modal-zen-deposit-address">
                            <i class="fa fa-qrcode"></i>
                        </button>
                        <b>{{ env('ZEN_DEPOSIT_ADDRESS') }}</b>
                    </div>

                    <div class="form-group">
                        <div class="mb-2">
                            <label for="setting-zen-amount" class="control-label">ZEN Amount</label>
                            <select name="zen_amount" id="setting-zen-amount" class="form-control">
                                <option value="0.01" {{ $zenAmount == '0.5' ? 'selected' : '' }}>$0.5</option>
                                <option value="2" {{ $zenAmount == '2' ? 'selected' : '' }}>$2</option>
                                <option value="5" {{ $zenAmount == '5' ? 'selected' : '' }}>$5</option>
                                <option value="10" {{ $zenAmount == '10' ? 'selected' : '' }}>$10</option>
                                <option value="custom" {{ $zenAmount == 'custom' ? 'selected' : '' }}>Custom Amount</option>
                            </select>
                        </div>

                        <div id="zen-amount-custom" style="display: none;">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">USD</span>
                                </div>
                                <input type="text" name="zen_amount_custom_usd" class="form-control" placeholder="" value="{{ $customUsdAmount }}"/>
                            </div>
                            <div class="text-center">OR</div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">ZEN</span>
                                </div>
                                <input type="text" name="zen_amount_custom_zen" class="form-control" placeholder="" value="{{ $customZenAmount }}"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="setting-event-name" class="control-label">Event name</label>
                        <input type="text" name="event_name" class="form-control" id="setting-event-name" placeholder="Event in which you're using the APP" value="{{ $eventName }}">
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-8 offset-2">
                                <button type="submit" class="btn btn-success btn-block">Save</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modal-zen-deposit-address" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Zen deposit address</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <img src="https://api.qrserver.com/v1/create-qr-code/?color=000000&amp;bgcolor=FFFFFF&amp;data={{ env('ZEN_DEPOSIT_ADDRESS') }}&amp;qzone=1&amp;margin=0&amp;size=400x400&amp;ecc=L" alt="qr code" class="img-fluid" />
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

<script type="text/javascript">

    $(function () {

        $("#setting-zen-amount").change(function () {
            if ($(this).val() === "custom") {
                $("#zen-amount-custom").show();
            } else {
                $("#zen-amount-custom").hide();
            }
        }).change();

        $.get("{{ action('MeetupsController@getBalance') }}", function (balance) {
            $("#account-balance").html(balance);
        });
    });

</script>

@endpush
