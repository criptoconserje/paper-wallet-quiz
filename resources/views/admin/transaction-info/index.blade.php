@extends('layouts.admin')

@section('content')

    <h3>List Transaction Info</h3>

    <div>
        <form action="{{ route('admin.transaction-info.index') }}">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Created between</label>
                        <input type="text" name="created_at_range" value="{{ request('created_at_range') }}"
                               data-input-from="#created_at_from"
                               data-input-to="#created_at_to"
                               class="form-control date-range-picker"/>
                        <input type="hidden" name="created_at_from" id="created_at_from"
                               value="{{ request('created_at_from') }}"/>
                        <input type="hidden" name="created_at_to" id="created_at_to"
                               value="{{ request('created_at_to') }}"/>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Email</label>
                        <input name="email" class="form-control" value="{{ request('email') }}"/>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>zen_address</label>
                        <input name="zen_address" class="form-control" value="{{ request('zen_address') }}"/>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>event_name</label>
                        <input name="event_name" class="form-control" value="{{ request('event_name') }}"/>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>team_member</label>
                        {{ Form::select(
                        'team_member',
                         \App\Models\User::pluck('username', 'username'),
                         request('event_name'),
                         ['placeholder' => 'All', 'class' => 'form-control']
                        ) }}
                    </div>
                </div>
            </div>
            <div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
            </div>
        </form>
    </div>

    <div class="row font-italic small mb-2">
        <div class="col-sm-3">
            COUNT: <b>{{ $models->total() }}</b>
        </div>
        <div class="col-sm-3">
            SUM(amount_given): <b>{{ $stats['sum'] }}</b>
        </div>
        <div class="col-sm-3">
            MIN(amount_given): <b>{{ $stats['min'] }}</b>
        </div>
        <div class="col-sm-3">
            MAX(amount_given): <b>{{ $stats['max'] }}</b>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">zen_address</th>
                <th scope="col">amount_given</th>
                <th scope="col">email</th>
                <th scope="col">event_name</th>
                <th scope="col">team_member</th>
                <th scope="col">created_at</th>
            </tr>
            </thead>
            <tbody>
            @foreach($models as $model)
                <tr>
                    <td>{{ $model->zen_address }}</td>
                    <td>{{ $model->amount_given }}</td>
                    <td>{{ $model->email_address_subscribed }}</td>
                    <td>{{ $model->event_name }}</td>
                    <td>{{ $model->team_member }}</td>
                    <td>{{ $model->created_at->format('Y-m-d H:i:s') }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div>
        {{ $models->appends(request()->all())->links('pagination::bootstrap-4') }}
    </div>

@endsection
