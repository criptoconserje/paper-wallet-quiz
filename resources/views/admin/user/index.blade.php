@extends('layouts.admin')

@section('content')


    <h3>List Admins</h3>

    <div>
        <form action="{{ route('admin.user.index') }}">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Created between</label>
                        <input type="text" name="created_at_range" value="{{ request('created_at_range') }}"
                               data-input-from="#created_at_from"
                               data-input-to="#created_at_to"
                               class="form-control date-range-picker"/>
                        <input type="hidden" name="created_at_from" id="created_at_from"
                               value="{{ request('created_at_from') }}"/>
                        <input type="hidden" name="created_at_to" id="created_at_to"
                               value="{{ request('created_at_to') }}"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Username</label>
                        <input name="username" class="form-control" value="{{ request('username') }}"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <div>
                            <label>
                                <input type="checkbox" value="1"
                                       name="is_admin" {{ request('is_admin') ? 'checked' : '' }}/>
                                is_admin
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
            </div>
        </form>
    </div>

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Username</th>
                <th scope="col">is_admin</th>
                <th scope="col">created_at</th>
                <th scope="col" style="width: 114px;">
                    <a href="{{ route('admin.user.create') }}" class="btn btn-success btn-sm btn-block">
                        <i data-feather="plus-square"></i>
                    </a>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{ $user->id }}</th>
                    <td>{{ $user->username }}</td>
                    <td>
                        @if($user->is_admin)
                            <span class="badge badge-success">ADMIN</span>
                        @else
                            <span class="badge badge-primary">user</span>
                        @endif
                    </td>
                    <td>{{ $user->created_at->format('Y-m-d H:i:s') }}</td>
                    <td>

                        <a href="{{ route('admin.user.edit', ['id' => $user->id]) }}" class="btn btn-primary btn-sm">
                            <i data-feather="edit"></i>
                        </a>
                        <button type="button" class="btn btn-sm btn-danger btn-delete"
                                data-confirm="Are you sure want to delete admin: {{ $user->username }}"
                                data-url="{{ route('admin.user.destroy', ['id' => $user->id]) }}">
                            <i data-feather="trash-2"></i>
                        </button>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div>
        {{ $users->appends(request()->all())->links('pagination::bootstrap-4') }}
    </div>

@endsection