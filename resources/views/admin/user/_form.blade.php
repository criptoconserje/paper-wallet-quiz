{{ isset($model)
 ? Form::model($model, ['route' => ['admin.user.update', $model->id], 'method' => 'PUT'])
 : Form::open(['route' => ['admin.user.store'], 'method' => 'post'])
}}

<div class="form-group">
    <label>
        Username <span class="text-danger">*</span>
    </label>
    {{ Form::text('username', null, ['class' => 'form-control' . ($errors->has('username') ? ' is-invalid' : '')]) }}
    @if ($errors->has('username'))
        <span class="invalid-feedback">{{ $errors->first('username') }}</span>
    @endif
</div>

<div class="form-group">
    <label>
        Password
        @if(isset($model))
            <small class="font-italic">Keep blank if you don't want to change</small>
        @else
            <span class="text-danger">*</span>
        @endif
    </label>
    {{ Form::password('password', ['class' => 'form-control' . ($errors->has('password') ? ' is-invalid' : '')]) }}
    @if ($errors->has('password'))
        <span class="invalid-feedback">{{ $errors->first('password') }}</span>
    @endif
</div>

<div class="form-group">
    <div class="form-check">
        {{ Form::checkbox('is_admin', 1, null, ['class' => 'form-check-input', 'id' => 'checkbox-is_admin']) }}
        <label class="form-check-label" for="checkbox-is_admin">
            is_admin
        </label>
    </div>
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary">{{ isset($model) ? 'Update' : 'Create' }}</button>
</div>

{{ Form::close() }}
