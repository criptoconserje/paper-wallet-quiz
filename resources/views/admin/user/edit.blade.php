@extends('layouts.admin')

@section('content')

    <h3>Update User {{ $model->username }}</h3>

    @include('admin.user._form')

@endsection
