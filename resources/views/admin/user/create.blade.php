@extends('layouts.admin')

@section('content')

    <h3>Create new User</h3>

    @include('admin.user._form')

@endsection
