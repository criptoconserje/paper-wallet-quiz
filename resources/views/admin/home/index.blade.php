@extends('layouts.admin')

@section('content')

    <div class="card mb-4">
        <h5 class="card-header">TOP 10 Transaction Info
            <small>group by <i>zen_address</i></small>
        </h5>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">zen_address</th>
                        <th scope="col">email</th>
                        <th scope="col">total_amount_given</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($topTransactionInfo as $value)
                        <tr>
                            <td>{{ $value['zen_address'] }}</td>
                            <td>{{ $value['email_address_subscribed'] }}</td>
                            <td>{{ $value['total_amount_given'] }}</td>
                            <td>
                                <a href="{{ route('admin.transaction-info.index', ['zen_address' => $value['zen_address']]) }}"
                                   class="btn btn-outline-primary btn-sm">Detail</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="card mb-4">
        <h5 class="card-header">TOP 10 Transaction Info
            <small>group by <i>event_name, team_member</i></small>
        </h5>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">event_name</th>
                        <th scope="col">team_member</th>
                        <th scope="col">total_amount_given</th>
                        <th scope="col">SUM(total_amount_given)</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($topTransactionInfoByEvent as $eventName => $transactionByAdmin)
                        @foreach($transactionByAdmin as $teamMember => $value)
                            <tr>
                                @if ($loop->parent->first)
                                    <td rowspan="{{ count($transactionByAdmin) }}">{{ $eventName }}</td>
                                @endif
                                <td>{{ $teamMember }}</td>
                                <td>{{ $value }}</td>
                                @if ($loop->parent->first)
                                    <td rowspan="{{ count($transactionByAdmin) }}">{{ array_sum($transactionByAdmin) }}</td>
                                @endif
                                <td>
                                    <a href="{{ route('admin.transaction-info.index', ['event_name' => $eventName, 'team_member' => $teamMember]) }}"
                                    class="btn btn-outline-primary btn-sm">Detail</a>
                                </td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
