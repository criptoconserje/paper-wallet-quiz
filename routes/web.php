<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// MEETUPS
Route::get('/', 'MeetupsController@index')->name('home');

Route::get('/get-balance', 'MeetupsController@getBalance');

Route::get('/login', 'MeetupsController@showLoginForm');
Route::post('/login', 'MeetupsController@login');
Route::get('/logout', 'MeetupsController@logout');

Route::get('/setting', 'MeetupsController@showSettingForm');
Route::post('/setting', 'MeetupsController@saveSetting');

Route::get('/subscribe', 'MeetupsController@showSubscribeForm');
Route::post('/subscribe', 'MeetupsController@subscribe');

Route::get('/claim', 'MeetupsController@showClaimForm');
Route::post('/claim', 'MeetupsController@claim');



// ADMIN
Route::middleware(['admin.login'])->namespace('Admin')->prefix('admin_ZwMTBv')->name('admin.')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::resource('user', 'UserController');

    Route::resource('transaction-info', 'TransactionInfoController');
});
