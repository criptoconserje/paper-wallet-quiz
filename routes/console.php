<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('tuanho:add-user', function () {

    $user = new \App\Models\User([
        'username' => 'tuanho',
        'password' => bcrypt('123456'),
        'is_admin' => 1,
    ]);

    $user->save();

    echo 'Done.', PHP_EOL;

})->describe('Add default user');
